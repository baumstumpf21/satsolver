package one;

import java.util.ArrayList;

public class CNF {
	
	private ArrayList<Clause> clauses;
	private int variableCount;
	private int clauseCount;
	
	public CNF() {
		clauses = new ArrayList<Clause>();
	}
	
	public boolean isSatisfied() {
		for(Clause clause : clauses) {
			if(!clause.isSatisfied()) return false;
		}
		return true;
	}
	
	public boolean isUnsatisfiable() {
		for(Clause clause : clauses) {
			if(clause.isUnsatisfiable()) {
				return true;
			}
		}
		return false;
	}
	
	public Clause getUnsatisfiable() {
		for(Clause clause : clauses) {
			if(clause.isUnsatisfiable()) {
				return clause;
			}
		}
		return null;
	}
	
	public boolean addClause(Clause clause) {
		boolean result = clauses.add(clause);
		clause.setNumber(clauses.indexOf(clause));
		return result;
	}
	
	public ArrayList<Clause> getClauses() {
		return clauses;
	}
	
	public int getVariableCount() {
		return variableCount;
	}

	public void setVariableCount(int variableCount) {
		this.variableCount = variableCount;
	}

	public int getClauseCount() {
		return clauseCount;
	}

	public void setClauseCount(int clauseCount) {
		this.clauseCount = clauseCount;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < clauses.size(); i++) {
			sb.append("(" + clauses.get(i) + ")");
			if(i < clauses.size() - 1) sb.append(" ^ ");
		}
		System.out.println(clauses.size());
		return sb.toString();
	}
	
}
