package one;

import java.util.ArrayList;
import java.util.LinkedList;

public class Clause {
	
	private static byte[] solution;
	private ArrayList<Literal> literals;
	private int number;
	
	public Clause() {
		literals = new ArrayList<Literal>();
	}
	
	public Clause(int number) {
		literals = new ArrayList<Literal>();
		this.number = number;
	}
	
	public boolean isSatisfied() {
		for(Literal literal : literals) {
			if((solution[literal.getVariable()] == 0 && literal.isNegative()) || (solution[literal.getVariable()] == 1 && !literal.isNegative())) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isUnsatisfiable() {
		for(Literal literal : literals) {
			if(solution[literal.getVariable()] == -1) {
				return false;
			}
			if((solution[literal.getVariable()] == 0 && literal.isNegative()) || (solution[literal.getVariable()] == 1 && !literal.isNegative())) {
				return false;
			}
		}
		return true;
	}
	
	public boolean addLiteral(Literal literal) {
		if(literals.contains(literal)) return false;
		else return literals.add(literal);
	}
	
	public ArrayList<Literal> getLiterals() {
		return literals;
	}
	
	/**
	 * Returns the first literal in this clause which is not set yet
	 * @return
	 */
	public Literal getFirstFreeLiteral() {
		for(Literal l : literals) {
			if(solution[l.getVariable()] == -1) return l;
		}
		return null;
	}
	
	public boolean isUnitClause() {
		return getFreeLiteralsCount() == 1 && !isSatisfied();
	}
	
	/**
	 * Return the number of free literals in this clause
	 * @return
	 */
	private int getFreeLiteralsCount() {
		LinkedList<Integer> vars = new LinkedList<Integer>();
		for(Literal l : literals) {
			if(solution[l.getVariable()] == -1) {
				vars.add(l.getVariable());
			}
		}
		return vars.size();
	}
	
	public static void setSolution(byte[] solution) {
		Clause.solution = solution;
	}
	
	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	public Clause getCopy() {
		Clause copy = new Clause();
		for(Literal l : getLiterals()) {
			copy.addLiteral(l);
		}
		copy.setNumber(getNumber());
		return copy;
	}

	public String toString() {
		String s = getNumber() + ": ";
		for(int i = 0; i < literals.size(); i++) {
			s += literals.get(i);
			if(i < literals.size() - 1) s += " v ";
		}
		return s;
	}

}
