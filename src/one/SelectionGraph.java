package one;

import java.util.LinkedList;

public class SelectionGraph implements ISelectionGraph {
	
	private LinkedList<Selection> tree;
	private byte[] solution;
	
	public SelectionGraph(byte[] solution) {
		tree = new LinkedList<Selection>();
		this.solution = solution;
	}
	
	public void addSelection(Selection selection) {
		tree.add(selection);
	}
	
	public boolean removeSelection(Selection selection) {
		return tree.remove(selection);
	}
	
	public Selection getSelection(int variable) {
		for(Selection selection : tree) {
			if(selection.getVariable() == variable) return selection;
		}
		return null;
	}
	
	public LinkedList<Selection> getSelections() {
		return tree;
	}
	
	/**
	 * returns all the selections of the highest decisionlevel
	 */
	public LinkedList<Selection> getHighestDecisionLevel() {
		int level = Selection.getCurrentSelectionLevel();
		LinkedList<Selection> selectionList = new LinkedList<Selection>();
		int i = tree.size() - 1;
		while(tree.size() > 0 && i >= 0) {
			if(tree.get(i).getSelectionLevel() == level) {
				selectionList.addFirst(tree.get(i));
			} else break;
			i--;
		}
		return selectionList;
	}
	
	/**
	 * Reverts all selections which have a higher decisionlevel then the given one
	 */
	public void revertUntilSelectionLevel(int decisionLevel) {
		while(!tree.isEmpty() && tree.getLast().getSelectionLevel() > decisionLevel) {
			Selection revert = tree.removeLast();
			solution[revert.getVariable()] = (byte) -1;
		}
		Selection.setCurrentSelectionLevel(decisionLevel);
	}
	
}