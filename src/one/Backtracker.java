package one;

import java.util.LinkedList;

public class Backtracker implements IBacktracker {
	
	private CNF cnf;
	private byte[] solution;
	private ISelectionGraph selectionGraph;
	private int conflictClausesLearned;
	
	public Backtracker(CNF cnf, byte[] solution, ISelectionGraph tree) {
		this.cnf = cnf;
		this.solution = solution;
		this.selectionGraph = tree;
		conflictClausesLearned = 0;
	}
	
	/**
	 * This is the main backtracking method
	 */
	public boolean backtrack(Clause clauseInConflict) {
		if(Selection.getCurrentSelectionLevel() < 1) return false;
		
		Clause resolve = clauseInConflict.getCopy();
		
		while(getNumberOfLiteralsSelectedOnCurrentSelectionLevel(resolve) > 1) {
			Selection lastSelection = getLastRelevantSelection(resolve);
			if(lastSelection == null) {
				System.err.println("Keine Selection zum Backtracken gefunden.");
				return false;
			}
			Clause implyingClause = cnf.getClauses().get(lastSelection.getImplyingClause());
			resolve = resolve(resolve, implyingClause, lastSelection.getVariable());
		}
		
		selectionGraph.revertUntilSelectionLevel(getSecondHighestDecisionLevel(resolve));
		
		cnf.addClause(resolve);
		conflictClausesLearned++;
		return true;
	}
	
	/**
	 * Gets the last selection made, which is relevant for the given clause
	 * @param clause
	 * @return
	 */
	private Selection getLastRelevantSelection(Clause clause) {
		LinkedList<Selection> selections = selectionGraph.getSelections();
		for(int i = selections.size() - 1; i >= 0; i--) {
			for(Literal lit : clause.getLiterals()) {
				if(selections.get(i).getVariable() == lit.getVariable()) {
					return selections.get(i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Returns the second highest decisionlevel of the literals in clause
	 * @param clause
	 * @return
	 */
	private int getSecondHighestDecisionLevel(Clause clause) {
		int highest = 0;
		int second = 0;
		for(Literal lit : clause.getLiterals()) {
			if(selectionGraph.getSelection(lit.getVariable()) == null) System.out.println("Selection ist Null: " + lit.getVariable());
			if(solution[lit.getVariable()] != -1) {
				Selection selection = selectionGraph.getSelection(lit.getVariable());
				int sl = selection.getSelectionLevel();
				if(sl > highest) {
					second = highest;
					highest = sl;
				}
			}
		}
		return second;
	}
	
	/**
	 * Returns the number of literals which got their values on the current dicisionlevel
	 * @param clause
	 * @return
	 */
	private int getNumberOfLiteralsSelectedOnCurrentSelectionLevel(Clause clause) {
		int result = 0;
		for(Literal lit : clause.getLiterals()) {
			for(Selection sel : selectionGraph.getHighestDecisionLevel()) {
				if(sel.getVariable() == lit.getVariable()) result++;
			}
		}
		return result;
	}
	
	/**
	 * Resolves clause c1 and c2 on the given variable and returns it as new clause
	 * @param c1
	 * @param c2
	 * @param variable
	 * @return
	 */
	public static Clause resolve(Clause c1, Clause c2, int variable) {
		Clause resolved = new Clause();
		for(Literal lit1 : c1.getLiterals()) {
			if(variable != lit1.getVariable()) resolved.addLiteral(lit1);
		}
		for(Literal lit2 : c2.getLiterals()) {
			if(variable != lit2.getVariable()) resolved.addLiteral(lit2);
		}
		LinkedList<Literal> removals = new LinkedList<Literal>();
		for(int i = 0; i < resolved.getLiterals().size(); i++) {
			for(int j = (i + 1); j < resolved.getLiterals().size(); j++) {
				Literal lit1 = resolved.getLiterals().get(i);
				Literal lit2 = resolved.getLiterals().get(j);
				if(lit1 != lit2 && lit1.getVariable() == lit2.getVariable() && lit1.isNegative() == lit2.isNegative()) {
					removals.add(lit2);
				}
			}
		}
		for(Literal lit : removals) {
			resolved.getLiterals().remove(lit);
		}
		return resolved;
	}
	
	public int getConflictClausesLearned() {
		return conflictClausesLearned;
	}
	
}
