package one;

import java.io.IOException;

public class Solver {
	
	public CNF cnf;
	public byte[] solution;
	public IBCP bcp;
	public ISelector selector;
	public ISelectionGraph selectionGraph;
	public IBacktracker backtracker;
	private boolean pause;
	private boolean print;
	
	public Solver(String[] args) {
		init(args);
	}
	
	public static void main(String[] args) {
		if(args.length >= 1) {
			Solver solver = new Solver(args);
			long start = System.currentTimeMillis();
			if(solver.solve()) {
				System.out.println(" - - - - - SAT - - - - - ");
				if(printSolution(args)) {
					for(int i = 0; i < solver.getSolution().length; i++) {
						System.out.println("Solver: Solution: " + i + " = " + solver.getSolution()[i]);
					}
					System.out.println(" - - - - - SAT - - - - - ");
				}
			} else {
				System.out.println(" - - - - - UNSAT - - - - - ");
			}
			long stop = System.currentTimeMillis();
			System.out.println("Milliseconds used: " + (stop - start));
			System.out.println(solver.getUnitClausesSolved() + " unit clauses solved.");
			System.out.println(solver.getSelectionCount() + " times selected.");
			System.out.println(solver.getConflictClausesLearned() + " conflict clauses learned.");
		}
	}
	
	/**
	 * starts to solve the cnf. it begins with a bcp and does a selection afterwards, if it has not finished, it starts the solveloop
	 * @return
	 */
	public boolean solve() {
		if(cnf.isSatisfied()) return true;
		if(cnf.isUnsatisfiable()) return false;
		pause("Start");
		while(bcp.solveUnitClause()) {
			if(cnf.isSatisfied()) return true;
			if(cnf.isUnsatisfiable()) return false;
		}
		pause("Nach erstem BCP-while - Vor erstem Select");
		if(!selector.select()) return false;
		pause("Nach erstem Select");
		if(cnf.isSatisfied()) return true;
		if(cnf.isUnsatisfiable()) return false;
		return solveLoop();
	}
	
	/**
	 * here is bcp done, if a conflict accures it is backtracked, otherwise a selection is made
	 * @return
	 */
	public boolean solveLoop() {
		while(true) {
			pause("SolveLoop-1 - Vor BCP");
			Clause clauseInConflict = null;
			while(clauseInConflict == null && bcp.solveUnitClause()) {
				if(cnf.isSatisfied()) return true;
				clauseInConflict = cnf.getUnsatisfiable();
				pause("SolveLoop-2 - While Schleife");
			}
			pause("SolveLoop-3 - Nach BCP - Vor Analyse - ClauseInConflict: " + clauseInConflict);
			if(clauseInConflict != null) {
				if(!backtracker.backtrack(clauseInConflict)) return false;
				pause("SolveLoop-4");
			} else {
				if(!selector.select()) return false;
				if(cnf.isSatisfied()) return true;
				if(cnf.isUnsatisfiable()) return false;
			}
		}
	}
	
	/**
	 * This method is used, when the -step or the -printall start option is used
	 * @param msg
	 */
	public void pause(String msg) {
		if(print) {
			System.out.println("Solver: " + msg);
			for(int i = 0; i < solution.length; i++) {
				System.out.println("Solver: Solution: " + i + " = " + solution[i]);
			}
			for(Clause clause : cnf.getClauses()) {
				System.out.println("Solver: " + clause + " is satisfied " + clause.isSatisfied());
			}
			for(Selection selection : selectionGraph.getSelections()) {
				System.out.println("Solver: Selection: " + selection);
			}
			if(pause) {
				try {
					System.in.read();
				} catch (IOException e) {
				}
			}
			System.out.println("----------------------------------------------------");
			System.out.println();
		}
	}
	
	public void init(String[] args) {
		print = printAll(args);
		DimacsReader dr = new DimacsReader(print || printDimacs(args));
		this.cnf = dr.readFromFile(args[0]);
		initSolution();
		Clause.setSolution(solution);
		selectionGraph = new SelectionGraph(solution);
		bcp = new BCP(cnf, solution, selectionGraph);
		setSelector(args);
		backtracker = new Backtracker(cnf, solution, selectionGraph);
		setPause(args);
	}
	
	private void setPause(String[] args) {
		for(String s : args) {
			if(s.toLowerCase().equals("-step")) {
				pause = true;
				print = true;
				return;
			}
		}
		pause = false;
	}
	
	private void setSelector(String[] args) {
		for(String s : args) {
			if(s.toLowerCase().equals("-jw")) {
				System.out.println("Using Jeroslaw-Wang.");
				selector = new Jeroslaw_Wang(cnf, solution, selectionGraph);
				return;
			}
			if(s.toLowerCase().equals("-naiveneg")) {
				System.out.println("Using naive negative Selector");
				selector = new NaiveNegativeSelector(cnf, solution, selectionGraph);
				return;
			}
			if(s.toLowerCase().equals("-naivepos")) {
				System.out.println("Using naive positive Selector");
				selector = new NaivePositiveSelector(cnf, solution, selectionGraph);
				return;
			}
		}
		System.out.println("Using standard Selector");
		selector = new StandardSelector(cnf, solution, selectionGraph);
	}
	
	private boolean printDimacs(String[] args) {
		for(String s : args) {
			if(s.toLowerCase().equals("-printdimacs")) return true;
		}
		return false;
	}
	
	private boolean printAll(String[] args) {
		for(String s : args) {
			if(s.toLowerCase().equals("-printall")) return true;
		}
		return false;
	}
	
	public void initSolution() {
		solution = new byte[cnf.getVariableCount()];
		for(int i = 0; i < solution.length; i++) {
			solution[i] = -1;
		}
	}
	
	private static boolean printSolution(String[] args) {
		for(String s : args) {
			if(s.toLowerCase().equals("-solution")) return true;
		}
		return false;
	}

	public CNF getCnf() {
		return cnf;
	}

	public byte[] getSolution() {
		return solution;
	}
	
	public int getConflictClausesLearned() {
		return backtracker.getConflictClausesLearned();
	}
	
	public int getSelectionCount() {
		return selector.getSelectionCount();
	}
	
	public int getUnitClausesSolved() {
		return bcp.getUnitClausesSolved();
	}
	
}