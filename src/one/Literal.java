package one;

public class Literal {
	
	private boolean isNegative;
	private int variable;
	
	public Literal(int varialble, boolean isNegative) {
		this.isNegative = isNegative;
		this.variable = varialble;
	}
	
	public int getLiteral() {
		if(isNegative) return -variable;
		else return variable;
	}
	
	public int getVariable() {
		return variable;
	}
	
	public boolean isNegative() {
		return isNegative;
	}
	
	public String toString() {
		if(isNegative) return "-" + variable;
		else return Integer.toString(variable);
	}

}
