package one;

import java.util.ArrayList;

public class Jeroslaw_Wang implements ISelector {
	
	private CNF cnf;
	private byte[] solution;
	private ISelectionGraph selectionGraph;
	private ArrayList<Tupel> J;
	private int selectionCount;
	
	public Jeroslaw_Wang(CNF cnf, byte[] solution, ISelectionGraph selectionGraph) {
		this.cnf = cnf;
		this.solution = solution;
		this.selectionGraph = selectionGraph;
		J = new ArrayList<Tupel>();
		long before = System.currentTimeMillis();
		init();
		long after = System.currentTimeMillis();
		System.out.println((after - before) + " milliseconds used for initialising. Beginning to solve.");
		selectionCount = 0;
	}
	
	/**
	 * In this method selections are made, when the -jw startoption is used
	 */
	public boolean select() {
		for(Tupel t : J) {
			if(solution[t.variable] == -1) {
				if(t.isNegative) {
					solution[t.variable] = 0;
					// Selection(int implyingClause, int variable, byte value, boolean newSelectionLevel)
					selectionGraph.addSelection(new Selection(-1, t.variable, (byte) 0, true));
				} else {
					solution[t.variable] = 1;
					// Selection(int implyingClause, int variable, byte value, boolean newSelectionLevel)
					selectionGraph.addSelection(new Selection(-1, t.variable, (byte) 1, true));
				}
				selectionCount++;
				return true;
			}
		}
		return false;
	}
	
	private void init() {
		for(int i = 0; i < solution.length; i++) {
			float pos = calcJ(new Literal(i, false));
			float neg = calcJ(new Literal(i, true));
			Tupel tupel;
			if(pos < neg) {
				tupel = new Tupel(pos, i, false);
			} else {
				tupel = new Tupel(neg, i, true);
			}
			insert(tupel);
		}
	}
	
	/**
	 * The given tupel is inserted into the sorted list of tupels
	 * @param p
	 */
	private void insert(Tupel p) {
		for(int i = 0; i < J.size(); i++) {
			if(p.J > J.get(i).J) {
				J.add(i, p);
				return;
			}
		}
		J.add(p);
	}
	
	/**
	 * The J value is calculatet for the given literal
	 * @param lit
	 * @return
	 */
	private float calcJ(Literal lit) {
		float j = 0;
		for(Clause clause : cnf.getClauses()) {
			for(Literal cl : clause.getLiterals()) {
				if(lit.getVariable() == cl.getVariable() && lit.isNegative() == cl.isNegative()) {
					j += Math.pow(2, -1 * clause.getLiterals().size());
					break;
				}
			}
		}
		return j;
	}
	
	/**
	 * This class is just a container for the J value, the variable and wether it is negative or not
	 * @author baumstumpf
	 *
	 */
	private class Tupel {
		public float J;
		public int variable;
		public boolean isNegative;
		
		public Tupel(float J, int variable, boolean isNegative) {
			this.J = J;
			this.variable = variable;
			this.isNegative = isNegative;
		}
	}

	@Override
	public int getSelectionCount() {
		return selectionCount;
	}
	
}
