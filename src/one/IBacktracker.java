package one;

public interface IBacktracker {
	
	boolean backtrack(Clause clauseInConflict);
	
	int getConflictClausesLearned();
	
}
