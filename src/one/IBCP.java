package one;

public interface IBCP {
	
	boolean solveUnitClause();
	
	int getUnitClausesSolved();
	
}
