package one;

public class NaiveNegativeSelector implements ISelector {
	
	private CNF cnf;
	private byte[] solution;
	private ISelectionGraph selectionGraph;
	private int selectionCount;
	
	public NaiveNegativeSelector(CNF cnf, byte[] solution, ISelectionGraph selectionGraph) {
		this.cnf = cnf;
		this.solution = solution;
		this.selectionGraph = selectionGraph;
		selectionCount = 0;
	}

	/**
	 * Here are selections made, when the -naiveneg start option is used
	 * The first unset variable is set to false
	 */
	public boolean select() {
		for(int i = 0; i < solution.length; i++) {
			if(solution[i] == -1) {
				solution[i] = 0;
				selectionGraph.addSelection(new Selection(-1, i, (byte) 0, true));
				selectionCount++;
				return true;
			}
		}
		return false;
	}

	public int getSelectionCount() {
		return selectionCount;
	}

}
