package one;

public class NaivePositiveSelector implements ISelector {
	
	private CNF cnf;
	private byte[] solution;
	private ISelectionGraph selectionGraph;
	private int selectionCount;
	
	public NaivePositiveSelector(CNF cnf, byte[] solution, ISelectionGraph selectionGraph) {
		this.cnf = cnf;
		this.solution = solution;
		this.selectionGraph = selectionGraph;
		selectionCount = 0;
	}

	/**
	 * Here are selections made, when the -naivepos start option is used
	 * The first unset variable is set to true
	 */
	public boolean select() {
		for(int i = 0; i < solution.length; i++) {
			if(solution[i] == -1) {
				solution[i] = 1;
				selectionGraph.addSelection(new Selection(-1, i, (byte) 1, true));
				selectionCount++;
				return true;
			}
		}
		return false;
	}

	public int getSelectionCount() {
		return selectionCount;
	}
	
}
