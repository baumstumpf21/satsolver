package one;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class DimacsReader {
	
	private int clauseCount;
	private int highestVariable;
	private boolean printOut;
	
	public DimacsReader(boolean print) {
		clauseCount = 0;
		printOut = print;
	}
	
	public CNF readFromFile(String path) {
		CNF cnf = new CNF();
	    try {
	    	BufferedReader br = new BufferedReader(new FileReader(path));
	        String line = br.readLine();
	        
	        while (line != null && !line.isEmpty()) {
	        	if(printOut) {
	        		System.out.println(line);
	        	}
	        	if(line.startsWith("c")) {
	        		line = br.readLine();
	        		continue;
	        	}
	        	if(line.startsWith("p")) {
	        		readProblemLine(line, cnf);
	        		line = br.readLine();
	        		continue;
	        	}
	        	for(Clause or : readClauseLine(line)) {
	        		cnf.addClause(or);
	        	}
	            line = br.readLine();
	        }
	        br.close();
	    } catch(FileNotFoundException e) {
	    	System.err.println("File not found.");
	    } catch(IOException e) {
	    	System.err.println("Cannot read from file.");
	    	System.err.println(e.getMessage());
	    	e.printStackTrace();
	    }
	    if(highestVariable + 1 != cnf.getVariableCount()) {
	    	System.out.println("Number of variables is not matching the number given in the problem line.");
	    	cnf.setVariableCount(highestVariable + 1);
	    }
	    if(clauseCount != cnf.getClauseCount()) {
	    	System.out.println("Number of clauses is not matching the number given in problem line: " + clauseCount + " != " + cnf.getClauseCount());
	    	cnf.setClauseCount(clauseCount);
	    }
	    return cnf;
	}
	
	private void readProblemLine(String line, CNF cnf) {
		String[] parts = line.split(" ");
		if(!parts[1].equals("cnf")) {
			System.out.println("The problem is not a cnf: " + parts[1]);
			System.exit(1);
		} else {
			cnf.setVariableCount(Integer.parseInt(parts[2]));
			cnf.setClauseCount(Integer.parseInt(parts[3]));
		}
	}
	
	private ArrayList<Clause> readClauseLine(String line) {
		String[] parts = line.split(" ");
		ArrayList<Clause> clauses = new ArrayList<Clause>();
		Clause clause = new Clause();
		clauseCount++;
		for(int i = 0; i < parts.length; i++) {
			if(parts[i].equals("0")) {
				clauses.add(clause);
				if(i < parts.length - 1) {
					clause = new Clause();
					clauseCount++;
				}
			} else {
				int literal = Integer.parseInt(parts[i]);
				int cutLiteral = Math.abs(literal) - 1;
				if(cutLiteral > highestVariable) highestVariable = cutLiteral;
				if(literal < 0) clause.addLiteral(new Literal(cutLiteral, true));
				else clause.addLiteral(new Literal(cutLiteral, false));
			}
		}
		return clauses;
	}
	
}
