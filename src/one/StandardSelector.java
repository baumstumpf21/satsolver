package one;

public class StandardSelector implements ISelector {
	
	private CNF cnf;
	private byte[] solution;
	private ISelectionGraph selectionGraph;
	private int selectionCount;
	
	public StandardSelector(CNF cnf, byte[] solution, ISelectionGraph selectionGraph) {
		this.cnf = cnf;
		this.solution = solution;
		this.selectionGraph = selectionGraph;
		selectionCount = 0;
	}
	
	/**
	 * Here are selections made when there is no start option used to select another one
	 */
	public boolean select() {
		Literal literal = null;
		for(Clause clause : cnf.getClauses()) {
			if(!clause.isSatisfied()) {
				literal = clause.getFirstFreeLiteral();
				break;
			}
		}
		if(literal != null) {
			if(literal.isNegative()) {
				solution[literal.getVariable()] = 0;
				// Selection(int implyingClause, int variable, byte value, boolean newSelectionLevel)
				selectionGraph.addSelection(new Selection(-1, literal.getVariable(), (byte) 0, true));
			}
			else {
				solution[literal.getVariable()] = 1;
				// Selection(int implyingClause, int variable, byte value, boolean newSelectionLevel)
				selectionGraph.addSelection(new Selection(-1, literal.getVariable(), (byte) 1, true));
			}
			selectionCount++;
			return true;
		}
		return false;
	}

	public int getSelectionCount() {
		return selectionCount;
	}
	
}
