package one;

import java.util.LinkedList;

public interface ISelectionGraph {
	
	void addSelection(Selection selection);
	
	boolean removeSelection(Selection selection);
	
	Selection getSelection(int variable);
	
	LinkedList<Selection> getSelections();
	
	LinkedList<Selection> getHighestDecisionLevel();
	
	void revertUntilSelectionLevel(int selectionLevel);
	
}
