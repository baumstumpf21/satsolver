package one;

public interface ISelector {
	
	boolean select();
	
	int getSelectionCount();

}
