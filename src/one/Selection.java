package one;

public class Selection {
	
	private int implyingClause;
	private int variable;
	private byte value;
	private static int currentSelectionLevel = 0;
	private int selectionLevel;
	
	public Selection(int implyingClause, int variable, byte value, boolean newSelectionLevel) {
		this.implyingClause = implyingClause;
		this.variable = variable;
		this.value = value;
		if(newSelectionLevel) currentSelectionLevel++;
		selectionLevel = currentSelectionLevel;
	}
	
	public int getImplyingClause() {
		return implyingClause;
	}
	
	public void setImplyingClause(int implyingClause) {
		this.implyingClause = implyingClause;
	}
	
	public int getVariable() {
		return variable;
	}
	
	public void setVariable(int variable) {
		this.variable = variable;
	}
	
	public byte getValue() {
		return value;
	}
	
	public void setValue(byte value) {
		this.value = value;
	}

	public static int getCurrentSelectionLevel() {
		return currentSelectionLevel;
	}

	public static void setCurrentSelectionLevel(int newSelectionLevel) {
		currentSelectionLevel = newSelectionLevel;
	}
	
	public int getSelectionLevel() {
		return selectionLevel;
	}
	
	public void setSelectionLevel(int selectionLevel) {
		this.selectionLevel = selectionLevel;
	}
	
	public static void decreseSelectionLevel() {
		currentSelectionLevel--;
	}
	
	public String toString() {
		return "Implying clause: " + implyingClause + ", variable: " + variable + ", value: " + value + ", selectionlevel: " + selectionLevel;
	}
	
}
