package one;

public class BCP implements IBCP {
	
	private CNF cnf;
	private byte[] solution;
	private ISelectionGraph selectionGraph;
	private int unitClausesSolved;
	
	public BCP(CNF cnf, byte[] solution, ISelectionGraph tree) {
		this.cnf = cnf;
		this.solution = solution;
		this.selectionGraph = tree;
		unitClausesSolved = 0;
	}
	
	/**
	 * this method searches and solves a unit clause and returns true if done so
	 */
	public boolean solveUnitClause() {
		Clause clause = null;
		for(Clause c : cnf.getClauses()) {
			if(c.isUnitClause()) {
				clause = c;
				break;
			}
		}
		if(clause == null) return false;
		Literal literal = clause.getFirstFreeLiteral();
		Selection selection;
		if(literal.isNegative()) {
			solution[literal.getVariable()] = 0;
			selection = new Selection(clause.getNumber(), literal.getVariable(), (byte) 0, false);
			selectionGraph.addSelection(selection);
		} else {
			solution[literal.getVariable()] = 1;
			selection = new Selection(clause.getNumber(), literal.getVariable(), (byte) 1, false);
			selectionGraph.addSelection(selection);
		}
		unitClausesSolved++;
		return true;
	}
	
	public int getUnitClausesSolved() {
		return unitClausesSolved;
	}
	
}
